//**********************************************************************************************************//
// Darragh Corrigan, September 2021
// Rotary Processor Project
// Accepts user input from 4x4 keypad and times a bipolar stepper motor to power a rotary film processor
// 4x4 keypad used ressitor divider network to drive a single analog pin (A1)
// Arduino Uno used
//**********************************************************************************************************//
// A Key used to confirm input and move to processing phase
// C key used to clear input during input phase, or cancel processing during processing phase
// C, D keys not used
//**********************************************************************************************************//


#include <LiquidCrystal.h>
#include <Stepper.h>


// Variables used for user interface
// The ADC values in keyVals and letterKeyVals must be check beforehand by reading out the ADC value when the pin is pressed.
// There is a spreadsheet to generate these arrays from the ADC readout values.
// The values used here are for the resistors used in my circuit.
char keyPress = ' ';            // Used to store the value read from the Numpad
int keyVals [10] = {790, 715, 558, 796, 724, 579, 801, 732, 597, 743};    // Array of values corresponding to each number key press
char keys [10] = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};      // Array of number keys, in order matching key values array
int letterKeyVals [6] = {488, 515, 539, 567, 807, 619};                   // Array of non-number values corresponding to each key press
char letterKeys [6] = {'A', 'B', 'C', 'D', '*', '#', };                   // Array of non-number keys, in order matching key values array
int keyTolerance = 1;      // Key input value plus/minus

// Variables used for timer setting
int adjustSeconds = 0;     // Determines whether minutes or seconds are being adjusted
String timerMins = "0";    // User input timer minutes value
String timerSecs = "0";    // User input timer seconds value
long timeInMillis = 0;     // Total timer time in mS
String timerDisplay = "";  // String used to pass back displayed timer value
long startTime = 0;        // Note the timer value at the start of processing
long timerCount = 0;       // Variable to hold the current timer value

// Both of the following variables could be combined into one. Two are used here for clarity
int filmProcessing = 0;    // Set to 1 to operate in the film processing phase
int timerSetting = 1;      // Set to 0 to operate in the user input phase

//Varibales used during film processing phase
const int stepsPerRevolution = 200; //Number of steps per output revolution
int processingCancelled = 0;        // Sets to 1 if processing is cancelled
long reverseCounter = 0;            // Reverse direction approximately every minute
int motorDirection = 1;             // Clockwise when positive, counterclockwise when negative
const int buzzer = 7;               // Digital pin used for buzzer



LiquidCrystal lcd(13, 12, 5, 4, 3, 2);                //  Create LCD instance and assign screen pins - may need to be updated depending on pin connections used
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);  //  Create instance of stepper and assign driving pins

void setup() {
  lcd.begin(16,2);            // Configure 16*2 LCD
  myStepper.setSpeed(100);    // Set stepper speed to 100rpm. Speed required depends on processor gearing
  pinMode(buzzer, OUTPUT);    // Buzzer is on digital pin
}

void loop() {
  ///////////////////////////////////////////////
  ////////        USER INPUT PHASE       ////////
  ///////////////////////////////////////////////
  while(timerSetting == 1)
  {
    setStepperIdle();         // Drive zeroes on all stepper pins to prevent overheating 
    keyPress = readNumPad();  // Readout numpad
    
    // LCD disply values stored in timer number and seconds variables
    lcd.clear();             
    lcd.setCursor(0,0);  
    lcd.print("*=Mins, #=Secs");
    lcd.setCursor(0,1);
    lcd.print(timerMins);
    lcd.print(" Mins, ");
    lcd.print(timerSecs);
    lcd.print(" Secs, ");


    if(keyPress == 'A')   // A key indicates user wants to move to processing phase
    {
      filmProcessing = 1;   // Now processing film
      timerSetting = 0;     // Not taking user input
      timeInMillis = (((timerMins.toInt())*60000) + ((timerSecs.toInt())*1000)); // Convert minutes and seconds to milliseconds for use in timer

      startTime = millis(); // Check the start time to compare the counter
      
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Film Processing");
      lcd.setCursor(0,1);
      lcd.print("Beginning");
      reverseCounter = millis(); // Initialise the reverse timer. This is used to determine when the motor should change direction

      delay(1000);          // Delay is mainly here to allow user enough time to read the output. Can be removed.
    }
    if(keyPress == 'C')     // If the user presses C, the timer input values are reset to 0
    {
      timerMins = "0";
      timerSecs = "0";
    }
    delay(250);             // Delay between polling of the numpad. If this is too short the buttons are read as multiples. Too slow and the UI is unresponsive.
  }
  
  
  ///////////////////////////////////////////////
  ////////         FILM PROCESSING       ////////
  ///////////////////////////////////////////////
  while(filmProcessing == 1)
  {   
    timerCount = timeInMillis - (millis()-startTime); // Calculate the remaining time in milliseconds
    
    if((millis()-reverseCounter) >= 60000)            // Check how long has elapsed since the last reversal
    {
      reverseCounter = millis();          // Re-initialise the reverse counter
      motorDirection = -1*motorDirection; // Reverse motor direction
      }

    timerDisplay = (convertMillisToMinsSecs(timerCount)); // Updte user display with remaining time
     
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Time Remaining:");
    lcd.setCursor(0,1);
    lcd.print(timerDisplay);

    myStepper.step(motorDirection*5*stepsPerRevolution);     // Run the motor for five revolutions, direction depends on direction variable 
    setStepperIdle();                     // Drive zeroes when motor is unused. Depending on power supply used this may not be necessary.
        
    delay(250);
    
   //if keyPress = C -> Cancel film processing and return to timer setting phase
    processingCancelled = checkForCancel();
    if(processingCancelled == 1)
    {
      setStepperIdle();   // Ensure that motor output is driving zeroes
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Cancelling");
      filmProcessing = 0;
      timerSetting = 1;
      delay(1000);       // No real need for this delay, just for UI clarity
    }

    //Check if processing is finished, return to user input mode and set off buzzer
   if((millis()-startTime) >= timeInMillis)
    {
      setStepperIdle();
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Film Processing");
      lcd.setCursor(0,1);
      lcd.print("Completed");
      filmProcessing = 0;
      timerSetting = 1;
      // Set off buzzer
      tone(buzzer, 880);
      delay(3000);
      noTone(buzzer); 
    }

  }
}

//**********************************************************************************************************//
// Functions
//**********************************************************************************************************//

char readNumPad()
{
  int keyIn = analogRead(A1);   // Read ADC on Analog Pin 1. Update pin for different connections
  char keyChar = ' ';           // Variable used to return input character

  for (int i=0; i<=9; i++) // Check if a number between 1 and 9 was pressed, by cycling through the possible ADC values
    {
      if(keyIn >= keyVals[i]-keyTolerance && keyIn <= keyVals[i]+keyTolerance) // If the ADC readout value is within range of the key value
      {
        keyChar = keys[i];
        if(adjustSeconds == 0)  // If user is inputing minute timer values
        {
          timerMins += keys[i]; // Concatenate number char to end of the timer value string
          if (timerMins.charAt(0)=='0') // Remove any leading zeroes
          {
           timerMins.remove(0, 1);
          }
        }
        else  // User is inputing secodns values
        {
          timerSecs += keys[i];
          if (timerSecs.charAt(0)=='0') // Delete leading zeroes
          {
           timerSecs.remove(0, 1);
          }
        }
      }
    }

  if(keyIn >= letterKeyVals[0]-keyTolerance && keyIn <= letterKeyVals[0]+keyTolerance) //Check if A was pressed
  {
    keyChar = confirmStart(); // Double check that user wants to start the processing
  }
  else if(keyIn >= letterKeyVals[2]-keyTolerance && keyIn <= letterKeyVals[2]+keyTolerance) //Check if C was pressed
  {
    keyChar = 'C'; // Return that C was pressed
  }
  else if(keyIn >= letterKeyVals[4]-keyTolerance && keyIn <= letterKeyVals[4]+keyTolerance) //Check if * was pressed
  {
    adjustSeconds = 0;  // Switch to adjusting minute values
    keyChar = '*';
  }
  else if(keyIn >= letterKeyVals[5]-keyTolerance && keyIn <= letterKeyVals[5]+keyTolerance) //Check if # was pressed
  {
    adjustSeconds = 1;  // Switch to adjusting second values
    keyChar = '#';
  }

  return(keyChar); // Return the key which was pressed
}

char confirmStart() // Confirm that the user intends to start processing
{
  char keyChar; // Character which was pressed
  int keyRead = 1023; // 1023 is the ADC readout when no button is pressed

  // Request user confirmation
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Confirm Start:");
  lcd.setCursor(0,1);
  lcd.print("A=Yes, Other=No");
  delay(2000);

  do{ // Keep polling the numpad until the user presses a button
    keyRead = analogRead(A1);
    delay(100);
  }while(keyRead==1023);

  if(keyRead >= letterKeyVals[0]-keyTolerance && keyRead <= letterKeyVals[0]+keyTolerance) //Check if A was pressed
  {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("A pressed");
    keyChar = 'A';
    delay(1000);
  }
  else
  {
    keyChar = ' ';
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("#=Mins, *=Secs"); // A wasnt pressed, return to normal user input phase
  }

  return(keyChar);
}

String convertMillisToMinsSecs(long timerCount) // Convert values in millisecond timer to minutes and seconds for display
{
  int mins = 0;
  int secs = 0;
  String returnString = "";

  mins = (timerCount / 60000);        // 1 minute = 60,000 milliseconds
  secs = ((timerCount % 60000)/1000); // 1 second = 10,000 milliseconds

  returnString = String(mins) + " Mins, " +String(secs) + " Secs"; // Prepare the values into a string for display

  return(returnString);
  }

int checkForCancel() // Confirm if user is cancelling film processing
{
  int cancelled = 0;
  int keyRead = analogRead(A1); // Check numberpad

  setStepperIdle(); // Drive zeroes on stepper motor. Probably not required
  
  if(keyRead >= letterKeyVals[2]-keyTolerance && keyRead <= letterKeyVals[2]+keyTolerance) //Check if C was pressed
  { // Confirm that user wants to cancel
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Confirm Cancel:");
    lcd.setCursor(0,1);
    lcd.print("A=Yes, Other=No");
    delay(2000);

    do{ // Poll numpad until user presses a key
      keyRead = analogRead(A1);
      delay(100);
    } while(keyRead==1023);

    if(keyRead >= letterKeyVals[0]-keyTolerance && keyRead <= letterKeyVals[0]+keyTolerance) //Check if A was pressed
   {
      cancelled = 1;
   }
  }
  return(cancelled);
  }

void setStepperIdle() // Drive all stepper pins with 0 to prevent current flow when motor is idle
{
  digitalWrite(8, LOW);
  digitalWrite(9, LOW);
  digitalWrite(10, LOW);
  digitalWrite(11, LOW);
}
